#!/bin/bash

echo -e "${c3}************************"    
echo -e      "*** Preparing System ***"
echo -e      "************************${c1}"

export LOCALE="fr_FR.UTF-8"
export TIMEZONE="Europe/Paris"

tst . ./config.sh

# Locales
out "Setting locales"
sed -i "s/# en_US.UTF-8/en_US.UTF-8/g" /etc/locale.gen
sed -i "s/# ${LOCALE}/${LOCALE}/g" /etc/locale.gen
tst locale-gen
tst dpkg-reconfigure -f noninteractive locales
echo "Language=${LOCALE}" > /etc/environment
tst . /etc/environment

# Timezone
out "Setting timezone"
tst ln -sf /usr/share/zoneinfo/${TIMEZONE} /etc/localtime
echo ${TIMEZONE} | tee /etc/timezone
tst dpkg-reconfigure -f noninteractive tzdata

# System upgrade
out "System upgrade"
tst apt update -qq
tst apt upgrade -yq
tst apt-get -yq autoremove --purge

# Dependencies
out "Installing dependencies"
tst apt -yq install curl dirmngr sudo git

# Shell
out "Setting bash options"
sed '/export LS_OPTIONS=/s/^# //g' -i ~/.bashrc
sed '/eval "`dircolors`"/s/^# //g' -i ~/.bashrc
sed '/alias ll=/s/^# //g' -i ~/.bashrc
sed '/alias l=/s/^# //g' -i ~/.bashrc
tst . ~/.bashrc