#!/bin/bash

# Colors
c1="\e[39m" # Normal
c2="\e[91m" # Red
c3="\e[92m" # Light Green
c4="\e[36m" # Cyan
c5="\e[31m" # Light Red
c6="\e[93m" # Yellow
c7="\e[32m" # Green
c8="\e[97m" # White

function tst {
    echo -e "${c7}   => $*${c4}"
    if ! $*; then
        echo -e "${c5}Exiting script due to error from: $*${c1}"
        exit 1
    fi
    echo -en "${c1}"
}

function out {
    if [ ! $CNT ]; then CNT=0; fi
    CNT=$((CNT+1))
    printf -v NUM "%02d" $CNT
    echo -e "${c3}${NUM} => $*${c1}"
}

# Determine absolute root script path
SOURCE="${BASH_SOURCE[0]}"

while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
    ROOT="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
    SOURCE="$(readlink "$SOURCE")"
    [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
ROOT="$( cd -P "$( dirname "$SOURCE" )" && pwd )"

echo -e "${c4}
00x.         'x0000000l.       .dKXXKKl 
WM0,         ;KMXOOOOOKO;    .:OX00000c 
WM0'         ;KWd.   .xMK,   ;XWd.....  
WM0,         ,KMo    .dMK,   '0WKxddo'  
WM0'         ;KMo    .dMK,    .cxkkkXNd.
MM0,         ;XMo    .dMK,         .dMN:
dKN0kkkkkc.  ;XMKxdddxK0:.   'xkkkkOXXd.
 'xNWWWWWx.  ;KWWWWWWNx.     ;XWWWWW0:${c1}"

echo -e "${c3}*******************************"    
echo -e      "*** CNIL PIA Tool installer ***"
echo -e      "*******************************${c1}"

scrdir=/tmp/pia-installer
mkdir -p ${scrdir}/scripts

tst wget -q https://bitbucket.org/lifeds/pia-installer-debian/raw/master/pia-installer.sh -O ${scrdir}/pia-installer.sh
tst wget -q https://bitbucket.org/lifeds/pia-installer-debian/raw/master/config.sh -O ${scrdir}/config.sh
tst wget -q https://bitbucket.org/lifeds/pia-installer-debian/raw/master/scripts/prepare.sh -O ${scrdir}/scripts/prepare.sh
tst wget -q https://bitbucket.org/lifeds/pia-installer-debian/raw/master/scripts/user.sh -O ${scrdir}/scripts/user.sh
tst wget -q https://bitbucket.org/lifeds/pia-installer-debian/raw/master/scripts/postgres.sh -O ${scrdir}/scripts/postgres.sh
tst wget -q https://bitbucket.org/lifeds/pia-installer-debian/raw/master/scripts/ruby.sh -O ${scrdir}/scripts/ruby.sh
tst wget -q https://bitbucket.org/lifeds/pia-installer-debian/raw/master/scripts/nodejs.sh -O ${scrdir}/scripts/nodejs.sh
tst wget -q https://bitbucket.org/lifeds/pia-installer-debian/raw/master/scripts/pia-back.sh -O ${scrdir}/scripts/pia-back.sh

cd ${scrdir}
. ${scrdir}/scripts/prepare.sh
. ${scrdir}/scripts/user.sh
. ${scrdir}/scripts/postgres.sh
. ${scrdir}/scripts/ruby.sh

# Cleanup 
out "Cleanup"
tst apt-get autoremove -qy --purge
tst apt-get clean -qy
#tst rm -rf /tmp/* /var/lib/apt/lists/* /var/tmp/*